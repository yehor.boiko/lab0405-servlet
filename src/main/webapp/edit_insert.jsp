<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String errorMessage = (String) request.getAttribute("errorMessage");
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Додати/редагувати резюме</title>
    <link rel="stylesheet" href="static/css/main.css">
</head>
<body>
<div class="container">
<div class="editAddForm content">
    <h1>Додати/редагувати резюме</h1>
    <c:if test="${not empty errorMessage}">
        <div class="error-message">${errorMessage}</div>
    </c:if>

    <div class="navigation">
        <a href="/" class="btn">Головна</a>
        <a href="ResumeController?action=listAll" class="btn">Переглянути всі резюме</a>
    </div>
    <form action="ResumeController" method="post">
        <input type="hidden" name="id" value="${Resume.id}">

        <div>
            <label for="lastName">Прізвище:</label>
            <input type="text" id="lastName" name="lastName" value="${Resume.lastName}" maxlength="32">
        </div>
        <div>
            <label for="firstName">Ім'я:</label>
            <input type="text" id="firstName" name="firstName" value="${Resume.firstName}" maxlength="16">
        </div>
        <div>
            <label for="middleName">Друге ім'я:</label>
            <input type="text" id="middleName" name="middleName" value="${Resume.middleName}" maxlength="32">
        </div>
        <div>
            <label for="phone">Телефон:</label>
            <input type="text" id="phone" name="phone" value="${Resume.phone}" maxlength="16">
        </div>

        <div>
            <label for="email">Email адреса:</label>
            <input type="text" id="email" name="email" value="${Resume.email}" maxlength="255">
        </div>
        <div>
            <label for="specialization">Спеціалізація:</label>
            <input type="text" id="specialization" name="specialization" value="${Resume.specialization}" maxlength="64">
        </div>
        <div>
            <label for="age">Вік:</label>
            <input type="number" id="age" name="age" value="${Resume.age}">
        </div>
        <div>
            <label for="gender">Стать:</label>
            <select id="gender" name="gender">
                <option value="Male" ${Resume.gender == 'Male' ? 'selected' : ''}>Male</option>
                <option value="Female" ${Resume.gender == 'Female' ? 'selected' : ''}>Female</option>
            </select>
        </div>

        <div>
            <label for="education">Освіта:</label>
            <input type="text" id="education" name="education" value="${Resume.education}" maxlength="255">
        </div>
        <div>
            <label for="work_experience">Досвід праці:</label>
            <input type="text" id="work_experience" name="workExperience" value="${Resume.workExperience}" maxlength="255">
        </div>
        <div>
            <label for="desired_salary">Бажана зарплата ($):</label>
            <input type="text" id="desired_salary" name="desiredSalary" value="${Resume.desiredSalary}">
        </div>
        <div>
            <label for="other">Інше:</label>
            <input type="text" id="other" name="other" value="${Resume.other}" maxlength="255">
        </div>

        <button type="submit" class="btn">Зберегти</button>
    </form>
</div>
</div>
</body>
</html>