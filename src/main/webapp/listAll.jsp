<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="edu.hneu.boikoyehor.lab0405.model.Resume" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Перегляд всіх резюме</title>
    <link rel="stylesheet" href="static/css/main.css">
</head>
<body>
<div class="container">
<div class="content">
    <h1>Резюме</h1>
    <div class="navigation">
        <a href="/" class="btn">Головна</a>
        <a href="ResumeController?action=insert" class="btn">Додати нове резюме</a>
    </div>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Прізвище</th>
            <th>Ім'я</th>
            <th>Друге ім'я</th>
            <th>Телефон</th>
            <th>Email адреса</th>
            <th>Спеціалізація</th>
            <th>Вік</th>
            <th>Стать</th>
            <th>Освіта</th>
            <th>Досвід праці</th>
            <th>Бажана зарплатня</th>
            <th>Інше</th>
            <th>Дата створення</th>
            <th>Дата зміни</th>
            <th></th>
        </tr>
        <c:forEach var="Resume" items="${Resumes}">
            <tr>
                <td>${Resume.id}</td>
                <td>${Resume.lastName}</td>
                <td>${Resume.firstName}</td>
                <td>${Resume.middleName}</td>
                <td>${Resume.phone}</td>
                <td>${Resume.email}</td>
                <td>${Resume.specialization}</td>
                <td>${Resume.age}</td>
                <td>${Resume.gender}</td>
                <td>${Resume.education}</td>
                <td>${Resume.workExperience}</td>
                <td>${Resume.desiredSalary}</td>
                <td>${Resume.other}</td>
                <td>${Resume.dateAdded}</td>
                <td>${Resume.dateModified}</td>
                <td>
                    <a href="ResumeController?action=edit&id=${Resume.id}">Редагувати</a>
                    <a href="ResumeController?action=delete&id=${Resume.id}">Видалити</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</div>
</body>
</html>