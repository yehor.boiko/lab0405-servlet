<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Головна сторінка</title>
  <link rel="stylesheet" href="static/css/main.css">
</head>
<body>
  <div class="container">
    <div class="content">
      <h1>Кадрове агенство</h1>
      <a href="ResumeController?action=listAll" class="btn">Переглянути всі резюме</a>
    </div>
  </div>
</body>
</html>