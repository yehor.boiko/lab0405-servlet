package edu.hneu.boikoyehor.lab0405.model;

import java.time.LocalDateTime;

public class ResumeModel {
    private Integer id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String phone;
    private String email;
    private String specialization;
    private Integer age;
    private String gender;
    private String education;
    private String workExperience;
    private Double desiredSalary;
    private String other;
    private LocalDateTime dateAdded;
    private LocalDateTime dateModified;

    public ResumeModel(String lastName, String firstName, String middleName, String phone, String email,
                       String specialization, Integer age, String gender, String education,
                       String workExperience, Double desiredSalary, String other, LocalDateTime dateAdded, LocalDateTime dateModified) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.phone = phone;
        this.email = email;
        this.specialization = specialization;
        this.age = age;
        this.gender = gender;
        this.education = education;
        this.workExperience = workExperience;
        this.desiredSalary = desiredSalary;
        this.other = other;
        this.dateAdded = dateAdded;
        this.dateModified = dateModified;
    }

    public ResumeModel() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    // Getters
    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getSpecialization() {
        return specialization;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getEducation() {
        return education;
    }

    public String getWorkExperience() {
        return workExperience;
    }

    public Double getDesiredSalary() {
        return desiredSalary;
    }

    public String getOther() {
        return other;
    }

    public LocalDateTime getDateAdded() {
        return dateAdded;
    }

    public LocalDateTime getDateModified() {
        return dateModified;
    }

    // Setters
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    public void setDesiredSalary(Double desiredSalary) {
        this.desiredSalary = desiredSalary;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public void setDateAdded(LocalDateTime dateAdded) {
        this.dateAdded = dateAdded;
    }

    public void setDateModified(LocalDateTime dateModified) {
        this.dateModified = dateModified;
    }

    @Override
    public String toString() {
        return "Resume{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", specialization='" + specialization + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", education='" + education + '\'' +
                ", workExperience='" + workExperience + '\'' +
                ", desiredSalary=" + desiredSalary +
                ", other='" + other + '\'' +
                ", dateAdded=" + dateAdded +
                ", dateModified=" + dateModified +
                '}';
    }
}