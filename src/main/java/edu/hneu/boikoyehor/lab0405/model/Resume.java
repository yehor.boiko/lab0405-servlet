package edu.hneu.boikoyehor.lab0405.model;

import jakarta.persistence.Entity;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Entity
@Table(
        name = "resume",
        uniqueConstraints = {
                @UniqueConstraint(name = "unique_email", columnNames = "email"),
        }
)
public class Resume {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "last_name", nullable = false)
    private String lastName;
    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "middle_name")
    private String middleName;
    @Column(name = "phone", nullable = false)
    private String phone;
    @Column(name = "email", nullable = false)
    private String email;
    @Column(name = "specialization", nullable = false)
    private String specialization;
    @Column(name = "age", nullable = false)
    private Integer age;
    @Column(name = "gender", nullable = false)
    private String gender;
    @Column(name = "education")
    private String education;
    @Column(name = "work_experience", nullable = false)
    private String workExperience;
    @Column(name = "desired_salary")
    private Double desiredSalary;
    @Column(name = "other", nullable = false)
    private String other;
    @Column(name = "date_added")
    @CreationTimestamp
    private LocalDateTime dateAdded;
    @Column(name = "date_modified")
    @UpdateTimestamp
    private LocalDateTime dateModified;

    public Resume(String lastName, String firstName, String middleName, String phone, String email,
                  String specialization, Integer age, String gender, String education,
                  String workExperience, Double desiredSalary, String other, LocalDateTime dateAdded, LocalDateTime dateModified) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.phone = phone;
        this.email = email;
        this.specialization = specialization;
        this.age = age;
        this.gender = gender;
        this.education = education;
        this.workExperience = workExperience;
        this.desiredSalary = desiredSalary;
        this.other = other;
        this.dateAdded = dateAdded;
        this.dateModified = dateModified;
    }

    public Resume() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    // Getters
    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getSpecialization() {
        return specialization;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getEducation() {
        return education;
    }

    public String getWorkExperience() {
        return workExperience;
    }

    public Double getDesiredSalary() {
        return desiredSalary;
    }

    public String getOther() {
        return other;
    }

    public LocalDateTime getDateAdded() {
        return dateAdded;
    }

    public LocalDateTime getDateModified() {
        return dateModified;
    }

    // Setters
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    public void setDesiredSalary(Double desiredSalary) {
        this.desiredSalary = desiredSalary;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public void setDateAdded(LocalDateTime dateAdded) {
        this.dateAdded = dateAdded;
    }

    public void setDateModified(LocalDateTime dateModified) {
        this.dateModified = dateModified;
    }

    @Override
    public String toString() {
        return "Resume{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", specialization='" + specialization + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", education='" + education + '\'' +
                ", workExperience='" + workExperience + '\'' +
                ", desiredSalary=" + desiredSalary +
                ", other='" + other + '\'' +
                ", dateAdded=" + dateAdded +
                ", dateModified=" + dateModified +
                '}';
    }
}