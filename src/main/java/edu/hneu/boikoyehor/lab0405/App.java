package edu.hneu.boikoyehor.lab0405;

import edu.hneu.boikoyehor.lab0405.model.Resume;
import edu.hneu.boikoyehor.lab0405.model.ResumeModel;
import edu.hneu.boikoyehor.lab0405.service.ResumeService;
import edu.hneu.boikoyehor.lab0405.util.Converter;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class App {
    public static void main(String[] args) {
        ResumeService resumeService = new ResumeService();
        // Resume newResume = new Resume("lastname", "firstname", "middlename", "45345435", "asdasd@gmail.com", "asdsa",
        //        5, "Male", "KHNEU", "work experience", 150000.0, "sdf");
        // Resume newResume = new Resume("", "", "", "", "", "",
        //        0, "", "", "", 0.0, "");
        // resumeService.persist(newResume);



        /* List<Resume> resumeList = resumeService.findAll();
        System.out.println("Усі резюме:");
        for(var resume : resumeList){
            System.out.println(resume);
        } */

        // var resume = resumeService.findById(1);
        // Resume resumeEntity = resumeService.findById(1);
        // System.out.println(resumeEntity);

        Resume resumeEntity = new Resume();
        Resume resumeEntityOld = resumeService.findById(1);



        // resumeModel.setDateAdded(LocalDateTime.now());
        // resumeEntity.setDateModified(LocalDateTime.now());

        // Converter.ResumeModelToResumeEntity(resumeModel, resumeEntity);
        resumeService.update(resumeEntity); // Обновляем существующую сущность в базе данных
    }
}