package edu.hneu.boikoyehor.lab0405.controller;

import edu.hneu.boikoyehor.lab0405.model.Resume;
import edu.hneu.boikoyehor.lab0405.model.ResumeModel;
import edu.hneu.boikoyehor.lab0405.service.ResumeService;
import edu.hneu.boikoyehor.lab0405.util.Converter;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet(urlPatterns = "/ResumeController")
public class ResumeController extends HttpServlet {
    private static final String EMAIL_REGEX = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
            + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
    private static String INSERT_OR_EDIT_RESUME = "/edit_insert.jsp";
    private static String LIST_RESUME = "/listAll.jsp";
    private ResumeService resumeService;

    public ResumeController() {
        super();
        resumeService = new ResumeService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String template = "";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("insert")) {
            template = INSERT_OR_EDIT_RESUME;
            request.setAttribute("Resumes", new ArrayList<>());
        } else if (action.equalsIgnoreCase("edit")) {
            int id = Integer.parseInt(request.getParameter("id"));

            // Check if the current resume id exists.
            if(!resumeService.isResumeExistsById(id)) {
                response.sendRedirect(request.getContextPath() + "/ResumeController?action=listAll");
                return;
            }

            template = INSERT_OR_EDIT_RESUME;
            Resume resume = resumeService.findById(id);
            request.setAttribute("Resume", resume);
        } else if (action.equalsIgnoreCase("delete")) {
            int id = Integer.parseInt(request.getParameter("id"));
            resumeService.delete(id);

            template = LIST_RESUME;
            List<Resume> resumeList = resumeService.findAll();
            request.setAttribute("Resumes", resumeList);
        } else {
            template = LIST_RESUME;
            List<Resume> resumeList = resumeService.findAll();

            request.setAttribute("Resumes", resumeList);
        }

        request.getRequestDispatcher(template).forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idValue = request.getParameter("id");
        Integer id = idValue != null && !idValue.isEmpty() ? Integer.parseInt(idValue) : null;

        // Process form data.
        ResumeModel resumeModel = new ResumeModel();
        resumeModel.setLastName(request.getParameter("lastName"));
        resumeModel.setFirstName(request.getParameter("firstName"));
        resumeModel.setMiddleName(request.getParameter("middleName"));
        resumeModel.setPhone(request.getParameter("phone"));
        resumeModel.setEmail(request.getParameter("email"));
        resumeModel.setSpecialization(request.getParameter("specialization"));
        resumeModel.setGender(request.getParameter("gender"));
        resumeModel.setEducation(request.getParameter("education"));
        resumeModel.setWorkExperience(request.getParameter("workExperience"));
        resumeModel.setOther(request.getParameter("other"));

        // Check if required fields are empty.
        if (resumeModel.getLastName() == null || resumeModel.getLastName().isEmpty()
                || resumeModel.getFirstName() == null || resumeModel.getFirstName().isEmpty()
                || resumeModel.getPhone() == null || resumeModel.getPhone().isEmpty()
                || resumeModel.getEmail() == null || resumeModel.getEmail().isEmpty()
                || resumeModel.getSpecialization() == null || resumeModel.getSpecialization().isEmpty()
                || resumeModel.getWorkExperience() == null || resumeModel.getWorkExperience().isEmpty()) {
            request.setAttribute("errorMessage", "Будь ласка, заповніть всі необхідні поля");
            if (id != null) {
                Resume resume = resumeService.findById(id);
                request.setAttribute("Resume", resume);
            }
            request.getRequestDispatcher(INSERT_OR_EDIT_RESUME).forward(request, response);
            return;
        }

        // Check field length.
        if (resumeModel.getLastName().length() > 32 || resumeModel.getFirstName().length() > 16
                || resumeModel.getMiddleName().length() > 32 || resumeModel.getPhone().length() > 16
                || resumeModel.getEmail().length() > 255 || resumeModel.getSpecialization().length() > 64
                || resumeModel.getEducation().length() > 255 || resumeModel.getOther().length() > 255) {
            request.setAttribute("errorMessage", "Деякі з полей перевищують максимально можливу довжину");
            if (id != null) {
                Resume resume = resumeService.findById(id);
                request.setAttribute("Resume", resume);
            }
            request.getRequestDispatcher(INSERT_OR_EDIT_RESUME).forward(request, response);
            return;
        }

        // Validate email.
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        Matcher matcher = pattern.matcher(resumeModel.getEmail());
        if(!matcher.matches()) {
            request.setAttribute("errorMessage", "Не валідний email");
            if (id != null) {
                Resume resume = resumeService.findById(id);
                request.setAttribute("Resume", resume);
            }
            request.getRequestDispatcher(INSERT_OR_EDIT_RESUME).forward(request, response);
            return;
        };
        if (resumeService.isEmailExists(resumeModel.getEmail(), id)) {
            request.setAttribute("errorMessage", "Даний email вже зареєстрований");
            if (id != null) {
                Resume resume = resumeService.findById(id);
                request.setAttribute("Resume", resume);
            }
            request.getRequestDispatcher(INSERT_OR_EDIT_RESUME).forward(request, response);
            return;
        }


        // Validate integer value after parsing.
        try {
            resumeModel.setAge(Integer.parseInt(request.getParameter("age")));
        } catch (NumberFormatException e) {
            request.setAttribute("errorMessage", "Неправильне значення віку (не ціле число)");
            if (id != null) {
                Resume resume = resumeService.findById(id);
                request.setAttribute("Resume", resume);
            }
            request.getRequestDispatcher(INSERT_OR_EDIT_RESUME).forward(request, response);
            return;
        }
        if(resumeModel.getAge() < 10) {
            request.setAttribute("errorMessage", "Вибачте, Ви ще занадто молодий");
            if (id != null) {
                Resume resume = resumeService.findById(id);
                request.setAttribute("Resume", resume);
            }
            request.getRequestDispatcher(INSERT_OR_EDIT_RESUME).forward(request, response);
        }

        // Validate double value after parsing.
        try {
            resumeModel.setDesiredSalary(Double.parseDouble(request.getParameter("desiredSalary")));
        } catch (NumberFormatException e) {
            request.setAttribute("errorMessage", "Невірне значення для бажаної зарплати (не число)");
            if (id != null) {
                Resume resume = resumeService.findById(id);
                request.setAttribute("Resume", resume);
            }
            request.getRequestDispatcher(INSERT_OR_EDIT_RESUME).forward(request, response);
            return;
        }

        // Add or update the resume.
        if (id == null) {
            // Add a new resume.
            Resume resumeEntity = new Resume();
            Converter.ResumeModelToResumeEntity(resumeModel, resumeEntity);
            resumeService.persist(resumeEntity);
        } else {
            // Update an existing resume.
            resumeModel.setId(id);
            Resume resumeEntity = resumeService.findById(id);
            if (resumeEntity == null) {
                response.sendRedirect(request.getContextPath() + "/ResumeController?action=listAll");
                return;
            }
            Converter.ResumeModelToResumeEntity(resumeModel, resumeEntity);
            resumeService.update(resumeEntity);
        }

        List<Resume> resumeList = resumeService.findAll();
        request.setAttribute("Resumes", resumeList);
        request.getRequestDispatcher(LIST_RESUME).forward(request, response);
    }
}
