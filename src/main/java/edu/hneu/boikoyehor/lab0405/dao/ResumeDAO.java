package edu.hneu.boikoyehor.lab0405.dao;
import java.util.List;

import edu.hneu.boikoyehor.lab0405.model.Resume;
import jakarta.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ResumeDAO implements ResumeDAOInterface<Resume, Integer> {
    private Session currentSession;
    private Transaction currentTransaction;
    public ResumeDAO(){

    }
    public Session openCurrentSession(){
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction(){
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(){
        currentSession.close();
    }

    public void closeCurrentSessionWithTransaction(){
        currentTransaction.commit();
        currentSession.close();
    }

    public SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration().configure("META-INF/hibernate.cfg.xml");
        return configuration.buildSessionFactory();
    }

    public Session getCurrentSession(){
        return currentSession;
    }
    public void setCurrentSession(Session currentSession){
        this.currentSession = currentSession;
    }

    public void setCurrentTransaction(Transaction transaction){
        this.currentTransaction = transaction;
    }

    @Override
    public void persist(Resume entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Resume entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Resume findById(Integer id) {
        Resume resume = (Resume) getCurrentSession().get(Resume.class, id);
        return resume;
    }

    @Override
    public void delete(Resume entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Resume> findAll() {
        List<Resume> resumes = (List<Resume>) getCurrentSession().createQuery("from Resume ").list();

        return resumes;
    }

    @Override
    public void deleteAll() {
        List<Resume> entityList = findAll();
        for(Resume entity : entityList){
            delete(entity);
        }
    }

    public boolean isResumeExistsById(Integer id) {
        Query query = getCurrentSession().createQuery("SELECT COUNT(*) FROM Resume WHERE id = :id", Long.class);
        query.setParameter("id", id);
        Long count = (Long) query.getSingleResult();
        return count > 0;
    }

    public boolean isEmailExistsForCreate(String email) {
        Query query = getCurrentSession().createQuery("SELECT COUNT(*) FROM Resume WHERE email = :email", Long.class);
        query.setParameter("email", email);
        Long count = (Long) query.getSingleResult();
        return count > 0;
    }

    public boolean isEmailExistsForUpdate(String email, Integer id) {
        Query query = getCurrentSession().createQuery("SELECT COUNT(*) FROM Resume WHERE email = :email AND id != :id", Long.class);
        query.setParameter("email", email);
        query.setParameter("id", id);
        Long count = (Long) query.getSingleResult();
        return count > 0;
    }

}
