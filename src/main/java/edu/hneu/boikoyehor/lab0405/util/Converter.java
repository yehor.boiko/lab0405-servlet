package edu.hneu.boikoyehor.lab0405.util;

import edu.hneu.boikoyehor.lab0405.model.Resume;
import edu.hneu.boikoyehor.lab0405.model.ResumeModel;

public class Converter {
    public static void ResumeEntityToResumeModel(Resume resumeEntity, ResumeModel resumeModel) {
        resumeModel.setId(resumeEntity.getId());
        resumeModel.setLastName(resumeEntity.getLastName());
        resumeModel.setFirstName(resumeEntity.getFirstName());
        resumeModel.setMiddleName(resumeEntity.getMiddleName());
        resumeModel.setPhone(resumeEntity.getPhone());
        resumeModel.setEmail(resumeEntity.getEmail());
        resumeModel.setSpecialization(resumeEntity.getSpecialization());
        resumeModel.setAge(resumeEntity.getAge());
        resumeModel.setGender(resumeEntity.getGender());
        resumeModel.setEducation(resumeEntity.getEducation());
        resumeModel.setWorkExperience(resumeEntity.getWorkExperience());
        resumeModel.setDesiredSalary(resumeEntity.getDesiredSalary());
        resumeModel.setOther(resumeEntity.getOther());
    }

    public static void ResumeModelToResumeEntity(ResumeModel resumeModel, Resume resumeEntity) {
        resumeEntity.setId(resumeModel.getId());
        resumeEntity.setLastName(resumeModel.getLastName());
        resumeEntity.setFirstName(resumeModel.getFirstName());
        resumeEntity.setMiddleName(resumeModel.getMiddleName());
        resumeEntity.setPhone(resumeModel.getPhone());
        resumeEntity.setEmail(resumeModel.getEmail());
        resumeEntity.setSpecialization(resumeModel.getSpecialization());
        resumeEntity.setAge(resumeModel.getAge());
        resumeEntity.setGender(resumeModel.getGender());
        resumeEntity.setEducation(resumeModel.getEducation());
        resumeEntity.setWorkExperience(resumeModel.getWorkExperience());
        resumeEntity.setDesiredSalary(resumeModel.getDesiredSalary());
        resumeEntity.setOther(resumeModel.getOther());
    }

}
