package edu.hneu.boikoyehor.lab0405.service;

import edu.hneu.boikoyehor.lab0405.dao.ResumeDAO;
import edu.hneu.boikoyehor.lab0405.model.Resume;

import java.util.List;

public class ResumeService {
    private static ResumeDAO resumeDAO;

    public ResumeService(){
        resumeDAO = new ResumeDAO();
    }
    public ResumeDAO resumeDAO () {
        return resumeDAO();
    }
    public void persist (Resume entity) {
        resumeDAO.openCurrentSessionWithTransaction();
        resumeDAO.persist(entity);
        resumeDAO.closeCurrentSessionWithTransaction();
    }
    public void update (Resume entity) {
        resumeDAO.openCurrentSessionWithTransaction();
        resumeDAO.update(entity);
        resumeDAO.closeCurrentSessionWithTransaction();
    }
    public Resume findById(Integer id) {
        resumeDAO.openCurrentSession();
        Resume resume = resumeDAO.findById(id);
        resumeDAO.closeCurrentSession();
        return resume;
    }
    public void delete (Integer id) {
        resumeDAO.openCurrentSessionWithTransaction();
        Resume declaration = resumeDAO.findById(id);
        resumeDAO.delete(declaration);
        resumeDAO.closeCurrentSessionWithTransaction();
    }
    public List<Resume> findAll() {
        resumeDAO.openCurrentSession();
        List<Resume> declarationList = resumeDAO.findAll();
        resumeDAO.closeCurrentSession();
        return declarationList;
    }
    public void deleteAll() {
        resumeDAO.openCurrentSessionWithTransaction();
        resumeDAO.deleteAll();
        resumeDAO.closeCurrentSessionWithTransaction();
    }

    public boolean isResumeExistsById(Integer id) {
        boolean result;
        resumeDAO.openCurrentSession();
        result = resumeDAO.isResumeExistsById(id);
        resumeDAO.closeCurrentSession();
        return result;
    }

    public boolean isEmailExists(String email, Integer id) {
        boolean result;
        resumeDAO.openCurrentSession();
        if (id == null) {
            result = resumeDAO.isEmailExistsForCreate(email);
        } else {
            result = resumeDAO.isEmailExistsForUpdate(email, id);
        }
        resumeDAO.closeCurrentSession();
        return result;
    }
}
